import { createClient } from "@supabase/supabase-js";
import { FileUploader } from "react-drag-drop-files";

const fileTypes = ["MP4", "MVI", "AVI", "JPG"];

const supabase = createClient(
  import.meta.env.VITE_APP_SUPABASE_URL,
  import.meta.env.VITE_APP_ANON_KEY
);

function App() {
  const handleChange = async (file) => {
    const { data, error } = await supabase.storage
      .from("Upload")
      .upload(`./${file.name}`, file);
    if (data) {
      console.log(data);
    } else {
      console.log(error);
    }
  };

  return (
    <div>
      <h2>upload your file here</h2>

      <FileUploader
        handleChange={(file) => handleChange(file)}
        name="video"
        types={fileTypes}
      />
    </div>
  );
}

export default App;




